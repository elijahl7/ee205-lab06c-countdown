///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Elijah Lopez <elijahl7@hawaii.edu>
// @date   15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define REFERENCE_TIME 1640995200

int
main ()
{

  time_t reference = REFERENCE_TIME;
  struct tm referenceStruct;
  char formattedTime[80];

  referenceStruct = *localtime (&reference);
  strftime (formattedTime, sizeof (formattedTime), "%a %b %d %X %p %Z %Y", &referenceStruct);
  printf ("Reference Time: %s\n", formattedTime);

  while (true)
    {
      time_t now;
      time (&now);
      now -= reference;

      struct tm timeSince;
      timeSince = *localtime (&now);

      printf ("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", timeSince.tm_year - 70, timeSince.tm_yday, timeSince.tm_hour, timeSince.tm_min, timeSince.tm_sec);

      sleep (1);
    }

  return 0;
}
